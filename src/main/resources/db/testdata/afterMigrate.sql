

lock tables aut_group write, aut_group_permission write, aut_permission write, aut_user write,
	aut_user_group write, oauth_code write, oauth_client_details write;

delete from aut_user;
delete from oauth_client_details;


insert into aut_user (name, email, password) values
('Rafael Gomes', 'nardnet@teste.com.br', '$2a$12$AnuSxNbCLS8rxhJj9gnKDevtapB3.DRuAfXf2xLRIyE61GPvRJPRa');




insert into oauth_client_details (
  client_id, resource_ids, client_secret, 
  scope, authorized_grant_types, web_server_redirect_uri, authorities,
  access_token_validity, refresh_token_validity, autoapprove
)
values (
  'nardnet-web', null, '$2a$12$AnuSxNbCLS8rxhJj9gnKDevtapB3.DRuAfXf2xLRIyE61GPvRJPRa',
  'READ,WRITE', 'password', null, null,
  60 * 60 * 6, 60 * 24 * 60 * 60, null
);

insert into oauth_client_details (
  client_id, resource_ids, client_secret, 
  scope, authorized_grant_types, web_server_redirect_uri, authorities,
  access_token_validity, refresh_token_validity, autoapprove
)
values (
  'nardnetanalytics', null, '$2a$12$AnuSxNbCLS8rxhJj9gnKDevtapB3.DRuAfXf2xLRIyE61GPvRJPRa',
  'READ,WRITE', 'authorization_code', 'http://www.localhost:8082', null,
  null, null, null
);

insert into oauth_client_details (
  client_id, resource_ids, client_secret, 
  scope, authorized_grant_types, web_server_redirect_uri, authorities,
  access_token_validity, refresh_token_validity, autoapprove
)
values (
  'nardnet', null, '$2a$12$AnuSxNbCLS8rxhJj9gnKDevtapB3.DRuAfXf2xLRIyE61GPvRJPRa',
  'READ,WRITE', 'client_credentials', null, null,
  null, null, null
);

unlock tables;