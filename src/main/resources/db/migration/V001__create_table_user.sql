create table aut_user (

 	id binary(16) NOT NULL DEFAULT (UUID_TO_BIN(UUID(), true)),
 	name varchar(60) not null,
 	email varchar(320) not null,
    password varchar(255) not null,

	primary key (id)
);


