create table aut_group (
	id binary(16) NOT NULL DEFAULT (UUID_TO_BIN(UUID(), true)),
	name varchar(60) not null,
	
	primary key (id)
) engine=InnoDB default charset=utf8;

create table aut_group_permission (
	group_id binary(16) NOT NULL,
	permission_id binary(16) NOT NULL,
	
	primary key (group_id, permission_id)
) engine=InnoDB default charset=utf8;

create table aut_permission (
	id binary(16) NOT NULL DEFAULT (UUID_TO_BIN(UUID(), true)),
	description varchar(60) not null,
	name varchar(100) not null,
	
	primary key (id)
) engine=InnoDB default charset=utf8;

create table aut_user_group (
	user_id binary(16) NOT NULL,
	group_id binary(16) NOT NULL,
	
	primary key (user_id, group_id)
) engine=InnoDB default charset=utf8;


alter table aut_group_permission add constraint fk_group_permission_permission
foreign key (permission_id) references aut_permission (id);

alter table aut_group_permission add constraint fk_group_permission_group
foreign key (group_id) references aut_group (id);

alter table aut_user_group add constraint fk_user_group_group
foreign key (group_id) references aut_group (id);

alter table aut_user_group add constraint fk_user_group_user
foreign key (user_id) references aut_user (id);