package com.nardnet.commons.authorization.infrastructure;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 * {@link ConfigurationProperties @ConfigurationProperties} for configuring NardnetCommons-authorization.
 *
 * @author Rafael Gomes 
 * @since 1.1.0
 */
@Validated
@Component
@ConfigurationProperties(prefix = "nardnetcommons.authorization")
public class AuthServerProperties {
	
	/**
	 * Signing Key to jwtAccessTokenConverter.	 
	 */
	private String signingKey;
	
	private boolean tokenStore = false;
	private boolean  accessTokenConverter = true;
	private boolean reuseRefreshTokens = false;

	public String getSigningKey() {
		return signingKey;
	}

	public void setSigningKey(String signingKey) {
		this.signingKey = signingKey;
	}

	public boolean isTokenStore() {
		return tokenStore;
	}

	public void setTokenStore(boolean tokenStore) {
		this.tokenStore = tokenStore;
	}

	public boolean isAccessTokenConverter() {
		return accessTokenConverter;
	}

	public void setAccessTokenConverter(boolean accessTokenConverter) {
		this.accessTokenConverter = accessTokenConverter;
	}

	public boolean isReuseRefreshTokens() {
		return reuseRefreshTokens;
	}

	public void setReuseRefreshTokens(boolean reuseRefreshTokens) {
		this.reuseRefreshTokens = reuseRefreshTokens;
	} 	

}
