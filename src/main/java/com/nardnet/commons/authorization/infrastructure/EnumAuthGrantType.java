package com.nardnet.commons.authorization.infrastructure;

public enum EnumAuthGrantType {
	
	AUTHORIZATION_CODE("authorization_code"),
	IMPLICIT("implicit"),	
	CLIENT_CREDENTIALS("client_credentials"),
	PASSWORD("password"),	
	REFRESH_TOKEN("refresh_token"),
	PKCE("pkce");	
	
	 public final String label;

	EnumAuthGrantType(String label) {
		this.label = label;
	}
	
	public static EnumAuthGrantType valueOfLabel(String label) {
	    for (EnumAuthGrantType e : values()) {
	        if (e.label.equals(label)) {
	            return e;
	        }
	    }
	    return null;
	}

}
