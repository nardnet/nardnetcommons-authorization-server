package com.nardnet.commons.authorization.infrastructure;

import java.util.Collection;
import java.util.Collections;
import java.util.UUID;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthoritiesContainer;
import org.springframework.security.core.userdetails.User;

public class AuthUser extends User {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private UUID userId;

	private String fullName;

	public AuthUser(com.nardnet.commons.authorization.domain.User user, Collection<? extends GrantedAuthority> authorities) {
		super(user.getEmail(), user.getPassword(), authorities);

		this.setFullName(user.getEmail());
		this.setUserId(user.getCode());
	}

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

}
