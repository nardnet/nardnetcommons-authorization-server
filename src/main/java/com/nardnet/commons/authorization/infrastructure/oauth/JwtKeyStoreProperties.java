package com.nardnet.commons.authorization.infrastructure.oauth;

import org.springframework.core.io.Resource;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
/**
 * {@link ConfigurationProperties @ConfigurationProperties} for configuring NardnetCommons-authorization.
 *
 * @author Rafael Gomes 
 * @since 1.1.0
 */
@Validated
@Component
@ConfigurationProperties("nardnetcommons.jwt.keystore")
public class JwtKeyStoreProperties {

	@NotNull
	private Resource jksLocation;
	
	@NotBlank
	private String password;
	
	@NotBlank
	private String keypairAlias;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getKeypairAlias() {
		return keypairAlias;
	}

	public void setKeypairAlias(String keypairAlias) {
		this.keypairAlias = keypairAlias;
	}

	public Resource getJksLocation() {
		return jksLocation;
	}

	public void setJksLocation(Resource jksLocation) {
		this.jksLocation = jksLocation;
	}

}
