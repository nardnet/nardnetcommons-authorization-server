package com.nardnet.commons.authorization.infrastructure.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

@Configuration
public class MessagesConfig {
	
	  @Bean
	  ResourceBundleMessageSource messageSource() {
	    ResourceBundleMessageSource rs = new ResourceBundleMessageSource();
	    rs.setBasename("classpath:");
	    rs.setDefaultEncoding("UTF-8");
	    rs.setUseCodeAsDefaultMessage(true);
	    return rs;
	  }

}
