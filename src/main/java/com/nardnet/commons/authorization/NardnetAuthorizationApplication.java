package com.nardnet.commons.authorization;

import java.util.TimeZone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.nardnet.commons.authorization.infrastructure.io.Base64ProtocolResolver;

@SpringBootApplication
@EnableAutoConfiguration
public class NardnetAuthorizationApplication {

	public static void main(String[] args) {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

		var app = new SpringApplication(NardnetAuthorizationApplication.class);
		app.addListeners(new Base64ProtocolResolver());
		app.run(args);
	}

}
