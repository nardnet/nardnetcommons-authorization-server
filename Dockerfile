FROM eclipse-temurin:17-jre-alpine

WORKDIR /app

COPY target/*.jar /app/apiserver.jar
COPY wait-for-it.sh /wait-for-it.sh

RUN chmod +x /wait-for-it.sh \
    && apk add --no-cache bash

EXPOSE 8081

CMD ["java", "-jar", "apiserver.jar", "--spring.profiles.active=prod" ]