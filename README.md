# NardnetCommons-authorization-server



## Getting started

## Project status
This project is used to send FAKE, SANDBOX, SMTP emails..

Very important to use of the dependency put in POM like this:

```
      <dependency>
			<groupId>com.nardnet</groupId>
			<artifactId>NardnetCommons-authorization-server</artifactId>
			<version>1.0.0-SNAPSHOT</version>
		</dependency>
		
	 <repositories>
		<repository>
			<id>gitlab-maven</id>
			<url>https://gitlab.com/api/v4/projects/53770600/packages/maven</url>
		</repository>
	 </repositories>

	<distributionManagement>
		<repository>
			<id>gitlab-maven</id>
			<url>https://gitlab.com/api/v4/projects/53770600/packages/maven</url>
		</repository>

		<snapshotRepository>
			<id>gitlab-maven</id>
			<url>https://gitlab.com/api/v4/projects/53770600/packages/maven</url>
		</snapshotRepository>
	</distributionManagement>
		
		
```

## Application.properties

server.port=8081

nardnetcommons.authorization.signing-key=

nardnetcommons.jwt.keystore.keypair-alias=

nardnetcommons.jwt.keystore.password=

nardnetcommons.jwt.keystore.jks-location=base64:sdjfksdjfkjsdkfjlsdjfie


spring.datasource.url=jdbc:mysql://localhost:3306/YOURDB?createDatabaseIfNotExist=true&serverTimezone=UTC

spring.datasource.username=

spring.datasource.password=

spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQL57Dialect

spring.flyway.locations=classpath:db/migration

spring.redis.host=

spring.redis.password=

spring.redis.port=6379

spring.session.store-type=redis


